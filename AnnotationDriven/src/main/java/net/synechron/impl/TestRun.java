package net.synechron.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


import net.synechron.models.Product;

public class TestRun {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();
		Product prod = new Product();
		prod.setId(234);
		prod.setProdname("Water Bottle");
		prod.setDesc("Milton Water Bottle 2.5 Litres");
		prod.setPrice(25.45f);
		
		session.persist(prod);
		transact.commit();
		System.out.println("Records saved in table");
		session.close();
		sfact.close();

		// Commit
	}
}
