package com.edforce;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/hello")
	public String redirect() {
		return "viewpage";
	}
	
	@RequestMapping("/helloguys")
	public String display() {
		return "helloguys";
	}

}
