package com.synechron;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class TestSpringApp {
public static void main(String[] args) {
	Resource res = new ClassPathResource("applicationContext.xml");
	BeanFactory fact = new XmlBeanFactory(res);
	
	Employee e = (Employee)fact.getBean("employee_bean");
	System.out.println(e.getEmpid());
	System.out.println(e.getName());
}
}
