package com.synechron.models;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value="Contract Employee")
public class C_Employee extends Employee{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="hourly_rate")
	private float hourlyrate;
	
	@Column(name="time_period")
	private int duration;
	
	public float getHourlyrate() {
		return hourlyrate;
	}
	public void setHourlyrate(float hourlyrate) {
		this.hourlyrate = hourlyrate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
