package com.synechron.models;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

@Entity
@DiscriminatorValue(value="Regular Employee")
public class R_Employee extends Employee{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="salary")
	private float Salary;

	public float getSalary() {
		return Salary;
	}

	public void setSalary(float salary) {
		Salary = salary;
	}
	
}
