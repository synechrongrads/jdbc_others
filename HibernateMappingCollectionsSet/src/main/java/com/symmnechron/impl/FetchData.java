package com.symmnechron.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Questions;

import jakarta.persistence.TypedQuery;

public class FetchData {
public static void main(String[] args) {
	//create a StandardServiceRegistry for registering the configuration
	StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
	//Build the Metadata
	Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
	// Create the sessionfactory
	SessionFactory sfact= meta.getSessionFactoryBuilder().build();
	// Create the session
	Session session = sfact.openSession();
	
	TypedQuery query = session.createQuery("from Questions");
	List<Questions> qlist = query.getResultList();
	int i = 1;
	Iterator<Questions> qitr = qlist.iterator();
	while(qitr.hasNext()) {
		Questions quest = qitr.next();
		System.out.println("Question " + i+": "+ quest.getQuest_name());
		Set<String>alist = quest.getQuest_options();
		Iterator<String> aitr = alist.iterator();
		while(aitr.hasNext()) {
			System.out.println(aitr.next());
		}
	}
}
}
