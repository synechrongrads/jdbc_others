package com.synechron.firstproject.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/displayproducts")
	public String productsDisplay() {
		return "displayproducts";
	}
}
