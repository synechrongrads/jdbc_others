package com.synechron.entity;

public class Employee {
private int id;
private String ename;
private String designation;
private int age;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getEname() {
	return ename;
}
public void setEname(String ename) {
	this.ename = ename;
}
public String getDesignation() {
	return designation;
}
public void setDesignation(String designation) {
	this.designation = designation;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
}
