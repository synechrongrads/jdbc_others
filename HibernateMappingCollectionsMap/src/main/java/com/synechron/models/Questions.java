package com.synechron.models;

import java.util.Map;

public class Questions {
	private int id;
	private String quest_name, askedby;
	private Map<String,String> quest_options;
	public Questions() {}
	public Questions(String quest_name, String askedby, Map<String, String> quest_options) {
		this.quest_name = quest_name;
		this.askedby = askedby;
		this.quest_options = quest_options;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuest_name() {
		return quest_name;
	}
	public void setQuest_name(String quest_name) {
		this.quest_name = quest_name;
	}
	
	public String getAskedby() {
		return askedby;
	}
	public void setAskedby(String askedby) {
		this.askedby = askedby;
	}
	public Map<String,String> getQuest_options() {
		return quest_options;
	}
	public void setQuest_options(Map<String,String> quest_options) {
		this.quest_options = quest_options;
	}

}