package com.synechron.impl;

import java.util.HashMap;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Questions;

public class StoreData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		
		HashMap<String, String> ans_map = new HashMap<String, String>();
		ans_map.put("Create an API and make the api calls in Angular.","Muthu");
		ans_map.put("Just Google it or ask ChatGPT for help","Muthu");
		ans_map.put("Create an API with GET, PUSH, POST and DELETE end points","Conor");
		ans_map.put("Enable Authguard to secure the endpoints from vulnerability", "Shiraz");
		Questions mhq = new Questions("How to develop enterprise application?", "Mehrsa", ans_map);
		
		session.persist(mhq);
		transact.commit();
		session.close();
		System.out.println("Mehrsa's Question is answered");
		
	}
}
