package com.synechron.impl;


import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Questions;

import jakarta.persistence.TypedQuery;

public class FetchData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();

		TypedQuery query = session.createQuery("from Questions");
		List<Questions> qlist = query.getResultList();

		Iterator<Questions> qitr = qlist.iterator();
		while(qitr.hasNext()) {
			Questions q = qitr.next();
			System.out.println(q.getQuest_name());
			System.out.println("Asked By: "+q.getAskedby());
			System.out.println();
			System.out.println("List of Answers: ");
			Map<String,String> ans_map = q.getQuest_options();
			Set<Map.Entry<String, String>> entset = ans_map.entrySet();

			Iterator<Map.Entry<String,String>> response = entset.iterator();
			while(response.hasNext()) {
				Map.Entry<String, String> entry = response.next();
				System.out.println("Response:"+ entry.getKey());
				System.out.println("Responded By:"+ entry.getValue());

			}

		}
		session.close();
	}
}
