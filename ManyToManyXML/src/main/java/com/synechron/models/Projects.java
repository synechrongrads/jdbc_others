package com.synechron.models;

import java.util.List;

public class Projects {

private int projectId;
private String projectName;
private float projectFunds;
private List<Employee> emplist;
public int getProjectId() {
	return projectId;
}
public void setProjectId(int projectId) {
	this.projectId = projectId;
}
public String getProjectName() {
	return projectName;
}
public void setProjectName(String projetName) {
	this.projectName = projetName;
}
public float getProjectFunds() {
	return projectFunds;
}
public void setProjectFunds(float projectFunds) {
	this.projectFunds = projectFunds;
}
public List<Employee> getEmplist() {
	return emplist;
}
public void setEmplist(List<Employee> emplist) {
	this.emplist = emplist;
}




}
