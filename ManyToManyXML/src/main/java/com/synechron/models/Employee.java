package com.synechron.models;

import java.util.List;

public class Employee {

	private int empid;
	private String ename,email;
	private List<Projects> projectlist;
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Projects> getProjectlist() {
		return projectlist;
	}
	public void setProjectlist(List<Projects> projectlist) {
		this.projectlist = projectlist;
	}
	
}
