package com.synechron.impl;


import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Projects;
import com.synechron.models.Employee;


public class SaveData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		Projects ecommerce = new Projects();
		ecommerce.setProjectName("Amazon");
		ecommerce.setProjectFunds(435645.56f);
		
		Projects bfsi = new Projects();
		bfsi.setProjectName("American Express");
		bfsi.setProjectFunds(234453.56f);
		
		Employee conor = new Employee();
		conor.setEname("Conor");
		conor.setEmail("conor@synechron.ca");
		ArrayList<Projects> con_projlist = new ArrayList<Projects>();
		con_projlist.add(ecommerce);
		conor.setProjectlist(con_projlist);
				
		Employee shiraz = new Employee();
		shiraz.setEname("Shriaz");
		shiraz.setEmail("shiraz@synechron.us");
		ArrayList<Projects> shiraz_projlist = new ArrayList<Projects>();
		shiraz_projlist.add(ecommerce);
		shiraz_projlist.add(bfsi);
		shiraz.setProjectlist(shiraz_projlist);
		
		Employee mehrsa = new Employee();
		mehrsa.setEname("Mehrsa");
		mehrsa.setEmail("mehrsa@synechron.eg");
		ArrayList<Projects> mehrsa_projlist = new ArrayList<Projects>();
		mehrsa_projlist.add(bfsi);
		mehrsa.setProjectlist(mehrsa_projlist);
		
		session.persist(conor);
		session.persist(shiraz);
		session.persist(mehrsa);
		transact.commit();
		session.close();
		System.out.println("Done Done Done");
	}
}
