import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ConnectToDB {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//Register the Driver
		Connection con = null;
		try {
			//Register the Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Connection URL
			// username password
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcdb","root","Pdntspa0!");
			//Creation of Statement
			Statement stmt= con.createStatement();
			String query = "Select * from employee";
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("EmpId\tEmpname\t\tSalary\t\tDesignation");
			while(rs.next()) {
				System.out.println(rs.getInt("id")+"\t"+rs.getString("empname")+
						"\t\t"+rs.getFloat("salary")+"\t\t"+rs.getString("designation"));
			}
			
			Scanner s = new Scanner(System.in);
			System.out.println("Enter the id");
			int id = s.nextInt();
			System.out.println("Enter new Salary");
			float sal = s.nextFloat();
			
//			String deletequery = "delete from employee where id="+ id;
//			int  result = stmt.executeUpdate(deletequery);
//			System.out.println(result + " row(s) affected");
			
//			String updatequery = "update employee set salary = " + sal + 
//					"where id = " + id;
//			int result = stmt.executeUpdate(updatequery);
//			System.out.println(result + " row(s) affected");
			
		}
		catch(SQLException e) {
			System.out.println(e.getMessage());

		}
		finally {
			con.close();
		}

	}
}
