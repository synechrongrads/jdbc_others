import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Prepared {
public static void main(String[] args) {
	Connection con = null;
	try {
		//Register the Driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// Connection URL
		// username password
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbcdb","root","Pdntspa0!");
		//Creation of Statement
		PreparedStatement ps =con.prepareStatement("insert into employee (empname,designation,salary)"
				+"values (?,?,?)");
		ps.setString(1, "Gajanan");
		ps.setString(2,"L&D Coordinator");
		ps.setFloat(3, 56783.45f);
		int result = ps.executeUpdate();
		System.out.println(result + " record(s) inserted");
	}
	catch(Exception e) {
		
	}
}
}
