package com.synechron.impl;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.synechron.models.Employee;

public class Test {
	public static void main(String[] args) {
		Resource res = new ClassPathResource("applicationContext.xml");
		BeanFactory fact = new XmlBeanFactory(res);
		
		Employee e1 = (Employee)fact.getBean("employee1");
		System.out.println(e1.getName());
	}
}
