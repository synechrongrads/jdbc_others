package com.synechron.models;

public class C_Employee extends Employee{
	private float hourlyrate;
	private int duration;
	public float getHourlyrate() {
		return hourlyrate;
	}
	public void setHourlyrate(float hourlyrate) {
		this.hourlyrate = hourlyrate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
