package net.synechron.models;

import java.util.List;

public class Question {
private int id;
private String quest;
private List<Answer> answerlist;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getQuest() {
	return quest;
}
public void setQuest(String quest) {
	this.quest = quest;
}
public List<Answer> getAnswerlist() {
	return answerlist;
}
public void setAnswerlist(List<Answer> answerlist) {
	this.answerlist = answerlist;
}
}
