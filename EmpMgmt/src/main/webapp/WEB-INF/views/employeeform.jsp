<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form:form method="POST" action="save">
<table>
	<tr>
		<td>Employee ID: </td>
		<td><form:input path="empid"/></td>
	</tr>
	<tr>
		<td>Employee Name: </td>
		<td><form:input path="empname"/></td>
	</tr>
	<tr>
		<td>Employee Salary: </td>
		<td><form:input path="salary"/></td>
	</tr>
	<tr>
		
		<td colspan="2"><input type="submit" value="Save Employee"/></td>
	</tr>
</table>
</form:form>

</body>
</html>