<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table{
border-collapse:collapse;
width:100%;
}
th,td{
	padding:12px;
	text-align:left;
	border-bottom: 1px solid #673d35; 
}

tr:hover{
background-color:  #ccfff2 ;
}
</style>
</head>
<body>
<table>
<tr>
<th>Employee No</th>
<th>Employee Name</th>
<th>Employee Salary</th>
<th colspan="2">Actions</th>
</tr>
<c:forEach var="empl" items="${emp_list}">
<tr>
<td>${empl.empid}</td>
<td>${empl.empname}</td>
<td>${empl.salary}</td>
<td><a href="#">update</a>
<td><a href="#">delete</a>
</tr>
</c:forEach>
</table>
</body>
</html>