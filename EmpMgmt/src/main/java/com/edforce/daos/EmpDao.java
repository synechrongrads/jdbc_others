package com.edforce.daos;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.edforce.models.Employee;

public class EmpDao {
	JdbcTemplate emptemplate;
	public void setTemplate(JdbcTemplate template) {
		this.emptemplate = template;
	}
	
	public int save(Employee e) {
		String sql_insert = "insert into Employee (empid,empname,salary) values"
				+ "("+e.getEmpid()+",'"+e.getEmpname()+"',"+e.getSalary()+");";
		return emptemplate.update(sql_insert);
	}
	
	public List<Employee> getAllEmployees() {
		String selectall = "select * from Employee";
		return emptemplate.query(selectall, new BeanPropertyRowMapper<Employee>(Employee.class));
	}
	
	public Employee getEmployeeById(int empid) {
		String select_one_emp = "select * from Employee where empid = ?";
		return emptemplate.queryForObject(select_one_emp, new BeanPropertyRowMapper<Employee>(Employee.class),empid);
	}
	
	public int updateEmployee(Employee e) {
		String udpateemp = "update Employee set empname=?, salary=? where empid=?";
		return emptemplate.update(udpateemp,new Object[] {
				e.getEmpname(),
				e.getSalary(),
				e.getEmpid()
		});
	}
	
	public int deleteEmployee(int empid) {
		String deleteemp = "delete from Employee where empid=?";
		return emptemplate.update(deleteemp,empid);
	}
}
