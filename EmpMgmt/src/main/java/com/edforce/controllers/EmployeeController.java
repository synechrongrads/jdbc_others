package com.edforce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.edforce.daos.EmpDao;
import com.edforce.models.Employee;

@Controller
public class EmployeeController {

	@Autowired
	EmpDao dao;
	
	@RequestMapping("/employeeform")
	public String employeeEntry(Model m) {
		m.addAttribute("command",new Employee());
		return "employeeform";
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public String saveRecords(@ModelAttribute("emp") Employee emp) {
		dao.save(emp);
		return "redirect:/viewEmployee";
	}
	
	@RequestMapping("/viewEmployee")
	public String viewEmpl() {
//		List<Employee> emplist = dao.getAllEmployees();
//		m.addAttribute("emp_list",emplist);
		return "viewEmployee";
	}
}
