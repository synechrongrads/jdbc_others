package com.synechron.models;

import java.util.List;

public class Questions {
private int id;
private String quest_name;
private List<String> quest_options;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getQuest_name() {
	return quest_name;
}
public void setQuest_name(String quest_name) {
	this.quest_name = quest_name;
}
public List<String> getQuest_options() {
	return quest_options;
}
public void setQuest_options(List<String> quest_options) {
	this.quest_options = quest_options;
}



}
/*
 * Hibernate Mapping File:
 * 
 * <key> is used to define the foreign key in the Answers table based on the quest
 * table.
 * 
 *  <index> this is used to identify the type (List or Map or Set)
 *  
 *  <element> define the elements of the collection
 *  
 *  <class name="com.synechron.models.Questions" table="quest_table">
 *  <id name="id">
 *  <generator class="increment></generator>
 *  </id>
 *  
 *  <property name="quest_name"></property>
 *  <list name="quest_options" table="answer_table">
 *  <key column="quid"></key>
 *  <index column="type"></index>
 *  <element column="answer">
 *  </list>
 *  </class>
 * 
 */