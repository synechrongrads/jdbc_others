package com.symmnechron.impl;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Questions;

public class StoreData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		ArrayList<String> q1_ans_list = new ArrayList<String>();
		q1_ans_list.add("1. Table Per Hierarchy");
		q1_ans_list.add("2. Two Tables per Hierarchy");
		q1_ans_list.add("3. Table per sub class");
		q1_ans_list.add("4. Table per abstract class");
		
		ArrayList<String> q2_ans_list = new ArrayList<String>();
		q2_ans_list.add("1. One to Many");
		q2_ans_list.add("2. Many to One");
		q2_ans_list.add("3. Many to Many");
		q2_ans_list.add("4. All mentioned");
		
		Questions q1 = new Questions();
		q1.setQuest_name("Which of the following is supported by Hibernate?");
		q1.setQuest_options(q1_ans_list);
		
		Questions q2 = new Questions();
		q2.setQuest_name("Supporting relationships in Hibernate are");
		q2.setQuest_options(q2_ans_list);
		
		session.persist(q1);
		session.persist(q2);
		
		transact.commit();
		session.close();
		System.out.println("Questions and answers added successfully");
		

	}
}
