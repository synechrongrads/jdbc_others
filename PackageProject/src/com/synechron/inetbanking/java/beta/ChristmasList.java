package com.synechron.inetbanking.java.beta;

import java.util.Iterator;
import java.util.LinkedList;

public class ChristmasList {
// Linked List:
/*It can have duplicate elements
 * It maintains insertion order
 * It is non-synchronized
 * Data handling is little faster compared to arraylist as it doesn't
 * involve shift operations
 * To store and modify the data
 * Please don't use linked list for random access
 * 
 * */
	
	public static void main(String[] args) {
		LinkedList<Integer> numlist = new LinkedList<Integer>();
		numlist.add(123);numlist.add(123);
		numlist.add(124);numlist.add(143);
		numlist.add(45);
		
		numlist.add(3,345);
		System.out.println(numlist);
		System.out.println(numlist.pop());
		System.out.println(numlist);
		System.out.println(numlist.peek());
		System.out.println(numlist);
		numlist.push(678);
		System.out.println(numlist);
		
		numlist.offer(97);
		System.out.println(numlist);
		numlist.offerFirst(100);
		System.out.println(numlist);
		numlist.offerLast(4567);
		System.out.println(numlist);
		System.out.println("Printing Linkedlist usign iterator");
		Iterator<Integer> itr = numlist.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}
