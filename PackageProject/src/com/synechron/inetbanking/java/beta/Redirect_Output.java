package com.synechron.inetbanking.java.beta;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class Redirect_Output {
	public static void main(String[] args) throws IOException {
		PrintStream ps = new PrintStream(new File("Redirected.txt"));
		PrintStream con = System.out;
		
		System.setOut(ps);
		System.out.println("This will be written inside the file");
		
		
		System.setOut(con);
		System.out.println("This will be displayed on the console");
	}
}
