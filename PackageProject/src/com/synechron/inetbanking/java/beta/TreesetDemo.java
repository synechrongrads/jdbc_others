package com.synechron.inetbanking.java.beta;

import java.util.TreeSet;

public class TreesetDemo {
	public static void main(String[] args) {
		TreeSet<Integer> myfavnum = new TreeSet<Integer>();
		myfavnum.add(45);
		myfavnum.add(23);
		myfavnum.add(11);
		myfavnum.add(56);
		myfavnum.add(12);
		myfavnum.add(8);
		myfavnum.add(34);
		System.out.println(myfavnum);
		
		System.out.println(myfavnum.ceiling(30));
		System.out.println(myfavnum.floor(30));
		System.out.println(myfavnum.higher(30));
		System.out.println(myfavnum.lower(30));
		
	}
}