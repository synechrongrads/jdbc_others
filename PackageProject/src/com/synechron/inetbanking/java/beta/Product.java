package com.synechron.inetbanking.java.beta;

import java.io.Serializable;

public class Product implements Serializable {
	static final long serialVersionUID = 1;
	int id;
	String prodname;
	String prod_desc;
	transient String manuf;
	
		public Product(int id, String prodname, String prod_desc, String manuf) {
		this.id = id;
		this.prodname = prodname;
		this.prod_desc = prod_desc;
		this.manuf = manuf;
	}

}
