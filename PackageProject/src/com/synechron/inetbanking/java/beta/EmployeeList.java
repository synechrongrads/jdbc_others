package com.synechron.inetbanking.java.beta;

import java.util.LinkedList;

public class EmployeeList {

	public static void main(String[] args) {
		Employee e1 = new Employee(10,"Modi");
		Employee e2 = new Employee(20,"Vladmir Putin");
		Employee e3 = new Employee(30,"Vladmir Putin");
		
		LinkedList<Employee> emplist = new LinkedList<Employee>();
		emplist.add(null);
		emplist.add(e1);
		emplist.add(e2);
		emplist.add(e3);
		emplist.add(1, e2);
		emplist.addFirst(new Employee(100, "Shiraz"));
		emplist.addLast(new Employee(140, "Conors"));
		
		
		// arraylist has incremental capacity - (default_capacity*3)/2 + 1
		// default capacity array list - 10
		// incremental capacity = 10 * 3 / 2 +1 = 16
		
		//linked list there is no such incremental capacity.
		//The available space in the memory is its size
		emplist.offerFirst(e3);
		emplist.offerLast(e2);
	}

}

class Employee{
	private int empid;
	private String name;

	Employee(int empid, String name){
		this.empid= empid;
		this.name=name;
	}

}

//create a menu driven code to perform CRUD operations on Employee objects
//that are present in the emplist.

/*
 * Display it in tabular format
 * Create employee class and add that many emp objects to the emplist
 * use the enhaned for loop to iterate the emplist
 * the input must be taken from the user 
 * */
