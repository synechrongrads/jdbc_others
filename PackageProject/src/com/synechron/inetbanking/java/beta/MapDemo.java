package com.synechron.inetbanking.java.beta;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class MapDemo {
	//HashMap, LinkedHashMap, TreeMap
	public static void main(String[] args) {
		HashMap<Integer, String> synechron_jersey = new HashMap<Integer, String>();
		synechron_jersey.put(10, "Bella");synechron_jersey.put(20, "Aashish");
		synechron_jersey.put(22, "Muthu");synechron_jersey.put(30, "Muthu");

		System.out.println(synechron_jersey);

		Iterator<Entry<Integer, String>> itr = synechron_jersey.entrySet().iterator();
		while(itr.hasNext()) {
			Entry<Integer, String> entry = itr.next();
			System.out.println(entry.getKey() +" | " + entry.getValue());
		}
		System.out.println("----------------------------");
		for(Entry<Integer, String> ent:synechron_jersey.entrySet()) {
			System.out.println(ent.getKey() +" - " + ent.getValue());
		} 

		System.out.println("----------------------------");
		for(Integer keys:synechron_jersey.keySet()) {
			System.out.println("Keys: " + keys);
		} 

		for(String values:synechron_jersey.values()) {
			System.out.println("Values: " + values);
		}
		System.out.println("----------------------------");
		synechron_jersey.forEach((k,v)->System.out.println("Key = " + k +" <-> Value = " + v));

	}
}

// Implement a simple shopping cart that is capable of adding products into the cart,deleting
// the product from th cart usign its id, Uodate basedon the prodid,display the items  


