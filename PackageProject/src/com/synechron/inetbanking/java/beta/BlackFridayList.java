package com.synechron.inetbanking.java.beta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BlackFridayList {

	public static void main(String[] args) {
		List l = new ArrayList();

		l.add("Welcome");
		l.add(10);
		l.add(34.65);
		l.add(true);

		//for,while,dowhile, enhanced forloop

		for(int i=0; i<l.size();i++) {
			System.out.println(l.get(i));
		}

		for(Object s : l) {
			System.out.println(s);
		}
		System.out.println("Size of the collection:" + l.size());
		System.out.println("Elements before remove");
		System.out.println(l);
		l.remove(true);
		System.out.println(l);
		l.remove(34.65);
		System.out.println(l);
		l.remove(Integer.valueOf(10));
		System.out.println(l);
		//cannot skip an index and add at a position
		l.add(1,10);
		System.out.println(l);
		System.out.println(l.isEmpty());
		System.out.println(l.contains(10));

		List k = new ArrayList(l);
		k.add("Monday Blues");
		System.out.println(k);

		List final_list = new ArrayList();
		final_list.add("Thank you");
		final_list.addAll(k);
		final_list.add(123);
		final_list.set(2,"Test");

		Object[] obj = final_list.toArray();

		for(Object ko:obj) {
			System.out.println(ko);
		}

	}

}
// Collection is a framework
// It is an abstraction
// Best design patterns and principles
// It makes use of Data Structure
// Stores group of values (Non-Primitive)
// Heterogeneous type of data

// Hierarchy
// Collection - Collections
// List, Set, Queue
// ArrayList, LinkedList, Vector
// HashSet, LinkedHashSet,Treeset
// DeQueue, PriorityQueue,ArrayDequeue
// Vector, Stack (Legacy classes)
// Map
// Hashmap,Treemap, LinkedHashmap
