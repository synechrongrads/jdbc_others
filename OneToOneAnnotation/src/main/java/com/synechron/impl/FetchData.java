package com.synechron.impl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Address;
import com.synechron.models.Employee;

import jakarta.persistence.TypedQuery;

public class FetchData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		
		TypedQuery query = session.createQuery("from Employee");
		List<Employee> empllist = query.getResultList();
		Iterator<Employee> empitr = empllist.iterator();
		while(empitr.hasNext()) {
			Employee e = empitr.next();
			System.out.println("Emp Id:" + e.getEmpid());
			System.out.println("Emp Name:" + e.getEname());
			System.out.println("Emp Email:" + e.getEmail());
			Address addr = e.getAddress();
			System.out.println("Emp City:" + addr.getCity());
			System.out.println("Emp State:" + addr.getState());
			System.out.println("Emp Country:" + addr.getCountry());
			System.out.println("Emp Zip Code:" + addr.getPincode());
		}
	}
}
