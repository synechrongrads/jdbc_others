package com.synechron.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name="c_emp_tpss")
public class C_Employee extends Employee{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float hourlyrate;
	private int duration;
	public float getHourlyrate() {
		return hourlyrate;
	}
	public void setHourlyrate(float hourlyrate) {
		this.hourlyrate = hourlyrate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
}
