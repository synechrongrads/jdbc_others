package com.synechron.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name="r_emp_tpss")
public class R_Employee extends Employee{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float Salary;

	public float getSalary() {
		return Salary;
	}

	public void setSalary(float salary) {
		Salary = salary;
	}
	
}
