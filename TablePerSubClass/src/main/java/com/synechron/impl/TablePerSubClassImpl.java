package com.synechron.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.C_Employee;
import com.synechron.models.Employee;
import com.synechron.models.R_Employee;

public class TablePerSubClassImpl {
public static void main(String[] args) {
	//create a StandardServiceRegistry for registering the configuration
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			//Build the Metadata
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
			// Create the sessionfactory
			SessionFactory sfact= meta.getSessionFactoryBuilder().build();
			// Create the session
			Session session = sfact.openSession();
			// Create Transaction
			Transaction transact = session.beginTransaction();
			
			Employee e = new Employee();
			e.setEname("Gajanan");e.setAge(26);
			
			R_Employee Shiraz = new R_Employee();
			Shiraz.setEname("Shiraz");
			Shiraz.setAge(23);
			Shiraz.setSalary(58965.45f);
			
			C_Employee muthu = new C_Employee();
			muthu.setEname("Muthu");
			muthu.setAge(38);
			muthu.setHourlyrate(80.5f);
			muthu.setDuration(50);
			
			session.persist(e);
			session.persist(Shiraz);
			session.persist(muthu);
			
			transact.commit();
			session.close();
			
			System.out.println("Data inserted successfully");
			
}
}
