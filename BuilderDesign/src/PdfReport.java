
public class PdfReport extends ReportBuilder{

	@Override
	public void setReportType() {
		reportObject.ReportType="Pdf Report";

	}

	@Override
	public void setReportHeader() {
		reportObject.ReportHeader = "Color Code Report for 2023";


	}

	@Override
	public void setReportContent() {
		reportObject.ReportContent = "Content in regular text format with images";

	}

	@Override
	public void setReportFooter() {
		reportObject.ReportFooter = "Approved by: Gajanan";

	}

	@Override
	public void FormatReport() {
		System.out.println("Formatting the data in HTML style");

	}
}
