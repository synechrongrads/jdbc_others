
public class HtmlReport extends ReportBuilder{

	@Override
	public void setReportType() {
		reportObject.ReportType="Html Report";

	}

	@Override
	public void setReportHeader() {
		reportObject.ReportHeader = "Color Code Report for 2023";


	}

	@Override
	public void setReportContent() {
		reportObject.ReportContent = "Content in HTML Format with links";

	}

	@Override
	public void setReportFooter() {
		reportObject.ReportFooter = "Approved by: Gajanan";

	}

	@Override
	public void FormatReport() {
		System.out.println("Formatting the data using bootstrap");

	}

}
