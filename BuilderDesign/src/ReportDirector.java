
public class ReportDirector {
	public Report MakeReport(ReportBuilder repbuild) {
		repbuild.createNewReport();
		repbuild.setReportType();
		repbuild.setReportHeader();
		repbuild.setReportContent();
		repbuild.FormatReport();
		repbuild.setReportFooter();
		return repbuild.getReport();
	}
}
