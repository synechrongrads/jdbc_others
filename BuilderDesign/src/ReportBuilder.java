
public abstract class ReportBuilder {
protected Report reportObject;
public abstract void setReportType();
public abstract void setReportHeader();
public abstract void setReportContent();
public abstract void setReportFooter();
public abstract void FormatReport();
public void createNewReport() {
	reportObject = new Report();
}
public Report getReport() {
	return this.reportObject;
}
}
