
public class Reportingrocess {
	public static void main(String[] args) {
		Report report;
		ReportDirector rd = new ReportDirector();
		ExcelReport report_excel = new ExcelReport();
		report = rd.MakeReport(report_excel);
		report.DisplayReport();
		System.out.println("----------------------------------");
		
		PdfReport report_pdf = new PdfReport();
		report = rd.MakeReport(report_pdf);
		report.DisplayReport();

		System.out.println("----------------------------------");
		
		HtmlReport report_html = new HtmlReport();
		report = rd.MakeReport(report_html);
		report.DisplayReport();
		
		
	}
}
