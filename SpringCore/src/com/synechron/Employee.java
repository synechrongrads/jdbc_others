package com.synechron;


public class Employee {
	private int empid;
	private String name;
	private String dept;
	private Address address;//Aggregation concept

	public Employee() {}
	public Employee(int empid) {
		this.empid = empid;
	}
	public Employee(String name) {
		this.name=name;
	}
	public Employee(int empid,String name, String dept, Address address) {
		this.empid = empid;
		this.name = name;
		this.dept = dept;
		this.address = address;
	}

	public void display() {
		System.out.println("Emp Id: " + this.empid+ " | " +" Employee Name: " + this.name +" | "+"Department: "+ this.dept +" | " + "Address: "+this.address);
	}

}
