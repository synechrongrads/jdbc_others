package com.synechron;

import java.util.Iterator;
import java.util.List;

public class Question {
private int id;
private String question;
private List<Answer> responselist;
public Question(int id, String question, List<Answer> responselist) {
	super();
	this.id = id;
	this.question = question;
	this.responselist = responselist;
}

public void display() {
	System.out.println("Question "+this.id+": "+this.question);
	System.out.println("\nChoices:");
	Iterator<Answer> resitr = responselist.iterator();
	while(resitr.hasNext()) {
		System.out.println(resitr.next());
	}
}

}
