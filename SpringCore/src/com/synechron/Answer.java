package com.synechron;

public class Answer {
	private int id;
	private String choice;
	
	public Answer(int id, String choice) {
		this.id = id;
		this.choice = choice;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + " " + this.choice;
	}
}
