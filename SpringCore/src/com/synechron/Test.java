package com.synechron;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Test {
public static void main(String[] args) {
	Resource resource = new ClassPathResource("applicationContext.xml");
	BeanFactory factory = new XmlBeanFactory(resource);
	
	Question quest = (Question)factory.getBean("questbean");
	quest.display();
//	Employee conors = (Employee)factory.getBean("conorsbean");
//	conors.display();
//	Employee mehrsa = (Employee)factory.getBean("mehrsabean");
//	mehrsa.display();
}
}
