
public class CreditCardFactory {

	public static CreditCard getCreditCard(String cardType) {
		CreditCard cc = null;
		switch(cardType) {

		case "MoneyBack":
			return new MoneyBack();
		case "Titanium":
			return new Titanium();
		case "Platinum":
			return new Platinum();
		default:
			return cc;
		}

	}
}
