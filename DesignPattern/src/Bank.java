import java.util.Scanner;

public class Bank {
	public static void main(String[] args) {
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter the Card Type to know the information");
		String cardType = scan.next();
		CreditCard cardDetails = CreditCardFactory.getCreditCard(cardType);
		
		if(cardDetails != null) {
			System.out.println("CardType: " + cardDetails.getCardType());
			System.out.println("Credit Limit: " + cardDetails.getCreditLimit());
			System.out.println("Annual Charges: " + cardDetails.getAnnualCharge());
		}
		else {
			System.out.println("Invalid Card Type");
		}
	}
}
