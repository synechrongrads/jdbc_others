package com.synechron.HibernateCriterianProject.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import com.synechron.HibernateCriterianProject.models.Employee;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

public class CriterianQuery {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		//Step 1: Create an object of Criteria Builder
		CriteriaBuilder  cb = session.getCriteriaBuilder();
		//Step 2: Create the criteria query object to reference the Persistence Class
		CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
		//Step 3: Create a root element to perform the select all query
		Root<Employee> root = cq.from(Employee.class);
		//Step 4: using the criteria query, apply the restrictions
		//Step 5: Display the records after restriction
		Query<Employee> allrecords_query = session.createQuery(cq);
		List<Employee> getallrecords = allrecords_query.getResultList();
		System.out.println("EmpId\t\tName\t\t\tDepartment\t\tSalary");
		for(Employee e: getallrecords) {
			System.out.println(e.getId()+"\t\t"+e.getName()+"\t\t\t"+e.getDept()+"\t\t"+e.getSal());
		}
		
		//Step 4: using the criteria query, apply the restrictions
		// fetch the records of the employees whose id >=2 and <= 6
		CriteriaQuery<Employee> gecq = cb.createQuery(Employee.class);
		Root<Employee> geroot = gecq.from(Employee.class);
		gecq.select(geroot).where(cb.and(cb.ge(geroot.get("id"), 2),cb.le(geroot.get("id"),6)));
		Query<Employee> select_few_records = session.createQuery(gecq);
		List<Employee> few_emp_list = select_few_records.getResultList();
		
		//Step 5: Display the records after restriction
		System.out.println("EmpId\t\tName\t\t\tDepartment\t\tSalary");
		for(Employee e: few_emp_list) {
			System.out.println(e.getId()+"\t\t"+e.getName()+"\t\t\t"+e.getDept()+"\t\t"+e.getSal());
		}
		
		// Lets select * from Emp where ename in ("Shiraz, Mehrsa, Conors, John Cena") order by sal asc
		CriteriaQuery<Employee> orderby_cq = cb.createQuery(Employee.class);
		Root<Employee> orderby_root = orderby_cq.from(Employee.class);
		orderby_cq.select(orderby_root)
		.where(orderby_root.get("name")
		.in("shiraz","mehrsa","conors","John Cena"))
		.orderBy(cb.asc(orderby_root.get("sal")));
		Query<Employee> orderbyquery = session.createQuery(orderby_cq);
		List<Employee> orderbylist = orderbyquery.getResultList();
		System.out.println("EmpId\t\tName\t\t\tDepartment\t\tSalary");
		for(Employee e: orderbylist) {
			System.out.println(e.getId()+"\t\t"+e.getName()+"\t\t\t"+e.getDept()+"\t\t"+e.getSal());
		}
		}
}
