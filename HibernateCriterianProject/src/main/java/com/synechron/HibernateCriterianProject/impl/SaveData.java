package com.synechron.HibernateCriterianProject.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.HibernateCriterianProject.models.Employee;

public class SaveData {
public static void main(String[] args) {
	//create a StandardServiceRegistry for registering the configuration
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			//Build the Metadata
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
			// Create the sessionfactory
			SessionFactory sfact= meta.getSessionFactoryBuilder().build();
			// Create the session
			Session session = sfact.openSession();
			// Create Transaction
			Transaction transact = session.beginTransaction();
			
			Employee e1 = new Employee("Shiraz", "DEV", 7454.56);
			Employee e2 = new Employee("Conors", "DEV", 7454.56);
			Employee e3 = new Employee("Mehrsa", "DEV", 7454.56);
			Employee e4 = new Employee("Muthu", "QA", 6454.56);
			Employee e5 = new Employee("Gajanan", "BA", 8454.56);
			Employee e6 = new Employee("Vladmir Putin", "QA", 9454.56);
			Employee e7 = new Employee("John Cena", "BA", 5454.56);
			Employee e8 = new Employee("The Rock", "DEV", 7454.56);
			
			session.persist(e1);session.persist(e2);session.persist(e3);
			session.persist(e4);session.persist(e5);session.persist(e6);
			session.persist(e7);session.persist(e8);
			transact.commit();
			session.close();
}
}
