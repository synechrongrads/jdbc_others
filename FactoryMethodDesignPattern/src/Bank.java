import java.util.Scanner;

public class Bank {
	public static void main(String[] args) {
		System.out.println("Enter the card type");
		Scanner sc = new Scanner(System.in);
		CreditCard cc = null;
		String cardType = sc.next();
		if(cardType.equals("Platinum")) {
			cc = new PlatinumFactory().CreateProduct();
			System.out.println(cc.getCardType());
			System.out.println(cc.getCreditLimit());
			System.out.println(cc.getAnnualFees());
		}
		else if(cardType.equals("MoneyBack")) {
			cc = new MoneyBackFactory().CreateProduct();
			System.out.println(cc.getCardType());
			System.out.println(cc.getCreditLimit());
			System.out.println(cc.getAnnualFees());
		}
		else if(cardType.equals("Titanium")) {
			cc = new TitaniumFactory().CreateProduct();
			System.out.println(cc.getCardType());
			System.out.println(cc.getCreditLimit());
			System.out.println(cc.getAnnualFees());
		}
	}
}
