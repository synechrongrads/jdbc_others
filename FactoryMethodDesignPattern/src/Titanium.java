
public class Titanium implements CreditCard{
	@Override
	public String getCardType() {
		return "Titanium";
	}

	@Override
	public int getCreditLimit() {
		return 25000;
	}

	@Override
	public int getAnnualFees() {
		return 1500;
	}

}
