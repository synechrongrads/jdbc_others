
public class Platinum implements CreditCard{
	@Override
	public String getCardType() {
		return "Platinum";
	}

	@Override
	public int getCreditLimit() {
		return 25000;
	}

	@Override
	public int getAnnualFees() {
		return 2000;
	}

}
