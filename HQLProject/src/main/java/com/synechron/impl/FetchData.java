package com.synechron.impl;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.MutationQuery;
import org.hibernate.query.Query;
import org.hibernate.query.spi.MutableQueryOptions;

import com.synechron.models.Employee;

import jakarta.persistence.TypedQuery;

public class FetchData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();

		Transaction tx = session.beginTransaction();
		System.out.println();
		System.out.println("Fetching All the Employee information using From");
		TypedQuery<Employee> query = session.createQuery("from Employee", Employee.class);
		List<Employee> emplist = query.getResultList();
		System.out.println();
		for(Employee e: emplist) {
			System.out.println("Employee Id: " + e.getId() + " | Employee Name: " 
					+ e.getName()+" | Employee Salary:" + e.getSalary()
					+ " | Employee Address: " + e.getAddress().getAddressline()+
					", "+ e.getAddress().getPlace()+", "+ e.getAddress().getPincode());

		}
		System.out.println();
		System.out.println("Fetching All the Employee information using Where clause");
		System.out.println();
		TypedQuery<Employee> table_filter = session.createQuery("from Employee where id= :id",Employee.class);
		table_filter.setParameter("id", 3);
		Employee e = table_filter.getSingleResult();
		System.out.println("Employee Id: " + e.getId() + " | Employee Name: " 
				+ e.getName()+" | Employee Salary:" + e.getSalary()
				+ " | Employee Address: " + e.getAddress().getAddressline()+
				", "+ e.getAddress().getPlace()+", "+ e.getAddress().getPincode());


		System.out.println();
		System.out.println("Applying Pagination");
		System.out.println();
		TypedQuery<Employee> pagination = session.createQuery("from Employee",Employee.class);
		pagination.setFirstResult(0);
		pagination.setMaxResults(2);

		List<Employee> emplist_paginated = pagination.getResultList();
		System.out.println();
		for(Employee emp: emplist_paginated) {
			System.out.println("Employee Id: " + emp.getId() + " | Employee Name: " 
					+ emp.getName()+" | Employee Salary:" + emp.getSalary()
					+ " | Employee Address: " + emp.getAddress().getAddressline()+
					", "+ emp.getAddress().getPlace()+", "+ emp.getAddress().getPincode());

		}

		System.out.println();
		System.out.println("Updating A single Employee");
		System.out.println();

		MutationQuery updateQuery = session.createMutationQuery("update Employee set name= :name where id= :id");
		updateQuery.setParameter("name", "Ashish Ambre");
		updateQuery.setParameter("id", 1);
		int result = updateQuery.executeUpdate();

		System.out.println("Updated the Employee table with " + result + " record(s)");

		TypedQuery<Employee> post_update_query = session.createQuery("from Employee",Employee.class);
		List<Employee> updated_emplist = post_update_query.getResultList();
		for(Employee emp: updated_emplist) {
			System.out.println("Employee Id: " + emp.getId() + " | Employee Name: " 
					+ emp.getName()+" | Employee Salary:" + emp.getSalary()
					+ " | Employee Address: " + emp.getAddress().getAddressline()+
					", "+ emp.getAddress().getPlace()+", "+ emp.getAddress().getPincode());

		}

		//Delete Employee
		// To delete the employee first it has to be deleted from address table and then only from emp table

		MutationQuery deleteQuery = session.createMutationQuery("delete from Address where id= :id");
		deleteQuery.setParameter("id", 4);
		deleteQuery.executeUpdate();

		deleteQuery = session.createMutationQuery("delete from Employee where id= :id");
		deleteQuery.setParameter("id", 4);
		deleteQuery.executeUpdate();

		tx.commit();

		// Aggregation

		TypedQuery<Employee> sum_sal = session.createQuery("select sum(salary) from Employee",Employee.class);
		System.out.println("Sum of all the salaries: "+ sum_sal.getSingleResult());
		TypedQuery<Employee> min_sal = session.createQuery("select min(salary) from Employee",Employee.class);
		System.out.println("Min of all the salaries: "+ min_sal.getSingleResult());
		TypedQuery<Employee> max_sal = session.createQuery("select max(salary) from Employee",Employee.class);
		System.out.println("Mx of all the salaries: "+ max_sal.getSingleResult());
		TypedQuery<Employee> avg_sal = session.createQuery("select avg(salary) from Employee",Employee.class);
		System.out.println("Average of all the salaries: "+ avg_sal.getSingleResult());
		TypedQuery<Employee> count_emp = session.createQuery("select count(id) from Employee",Employee.class);
		System.out.println("Count of employees: "+ count_emp.getSingleResult());

		// Creating joins using hibernate.query.Query

		//		Query join_query = session.createQuery("select e.name, a.place from Employee e"+
		//											   "INNER JOIN e.address a");
		//		List<Object[]> list = join_query.list();
		//		for(Object[] arr:list) {
		//			System.out.println(Arrays.toString(arr));
		//		}

		//Group By
		Query groupby = session.createQuery("select e.name,sum(e.salary),count(e)" + 
				"from Employee e where e.name like '%a_' group by e.name", Employee.class);
		List<Object[]> grouplist = groupby.getResultList();
		for(Object[] arr:grouplist) {
			System.out.println(Arrays.toString(arr));
		}

		Query orderby = session.createQuery("from Employee e order by e.id desc",Employee.class);
		List<Employee> desclist = orderby.getResultList();
		for(Employee empl : desclist) {
			System.out.println("Employee Id: " + empl.getId() + " | Employee Name: " 
					+ empl.getName()+" | Employee Salary:" + empl.getSalary()
					+ " | Employee Address: " + empl.getAddress().getAddressline()+
					", "+ empl.getAddress().getPlace()+", "+ empl.getAddress().getPincode());
		}


		session.close();
		sfact.close();


	}
}
