package net.synechron.models;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderColumn;
import jakarta.persistence.Table;

@Entity
@Table(name="quest_one_to_many_annotate")


public class Question {
@Id
@GeneratedValue(strategy = GenerationType.TABLE)
private int id;

private String quest;

@OneToMany(cascade = CascadeType.ALL)
@JoinColumn(name="quest_id")
@OrderColumn(name="list_index")
private List<Answer> answerlist;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getQuest() {
	return quest;
}
public void setQuest(String quest) {
	this.quest = quest;
}
public List<Answer> getAnswerlist() {
	return answerlist;
}
public void setAnswerlist(List<Answer> answerlist) {
	this.answerlist = answerlist;
}
}
