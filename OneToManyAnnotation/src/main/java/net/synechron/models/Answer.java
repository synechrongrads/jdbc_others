package net.synechron.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="answer_one_to_many_annotate")
public class Answer {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private int id;
private String response;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getResponse() {
	return response;
}
public void setResponse(String response) {
	this.response = response;
}


}
