package net.synechron.impl;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import net.synechron.models.Answer;
import net.synechron.models.Question;

public class SaveData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		Answer ans1 = new Answer();
		ans1.setResponse("Table Per Hierarchy");

		Answer ans2 = new Answer();
		ans2.setResponse("Two Tables per Hierarchy");

		Answer ans3 = new Answer();
		ans3.setResponse("Table per sub class");

		Answer ans4 = new Answer();
		ans4.setResponse("Table per sub class");

		ArrayList<Answer> anslist = new ArrayList<Answer>();
		anslist.add(ans1);anslist.add(ans2);anslist.add(ans3);anslist.add(ans4);

		Question q1 = new Question();
		q1.setQuest("Which of the following is supported by Hiberanate?");
		q1.setAnswerlist(anslist);

		session.persist(q1);

		transact.commit();
		session.close();
		System.out.println("Done Done Done");
	}
}
