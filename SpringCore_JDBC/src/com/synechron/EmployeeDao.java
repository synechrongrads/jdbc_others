package com.synechron;

import org.springframework.jdbc.core.JdbcTemplate;

public class EmployeeDao {
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	// save new employee record
	public int  saveEmployee(Employee e) {
		String insert_query = "insert into employee values ("+e.getId()+", '"+e.getName()+"',"+e.getSalary()+");";
		return jdbcTemplate.update(insert_query);
	}
	// udpate existing employee record
	public int  updateEmployee(Employee e) {
		String update_query = "update employee set name="+"'"+e.getName()+
				"', salary="+e.getSalary()+" where id="+e.getId()+");";
		return jdbcTemplate.update(update_query);
	}


	// delete the employee record
	public int  deleteEmployee(int id) {
		String delete_query = "delete from employee where id = "+id+";";
		return jdbcTemplate.update(delete_query);
	}
}
