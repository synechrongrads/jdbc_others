package com.synechron;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

public class Test {
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		EmployeeDao dao = (EmployeeDao)ctx.getBean("empdao");
//		Employee e = new Employee(99,"Tom Cruise",8545.56f);
//		int status = dao.saveEmployee(e);
//		System.out.println("Successfully inserted " + status + " record(s)");
		
//		int updatestatus = dao.updateEmployee(new Employee(99,"James Bond",8545.56f));
//		System.out.println("Updated "+ updatestatus + " record(s)");
		
		
//		Employee e = new Employee(120, "Gajanan", 5678);
//		dao.saveEmployee(e); 		
		int delete_status = dao.deleteEmployee(99);
		System.out.println("Deleted  " + delete_status + " record(s)");
	}
}
