package com.synechron.impl;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.synechron.models.Address;
import com.synechron.models.Employee;


public class SaveData {
	public static void main(String[] args) {
		//create a StandardServiceRegistry for registering the configuration
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		//Build the Metadata
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		// Create the sessionfactory
		SessionFactory sfact= meta.getSessionFactoryBuilder().build();
		// Create the session
		Session session = sfact.openSession();
		// Create Transaction
		Transaction transact = session.beginTransaction();

		Employee conor = new Employee();
		conor.setEname("Conor");
		conor.setEmail("conor@synechron.ca");
		Address con_add = new Address();
		con_add.setCity("toranto");
		con_add.setState("Ontario");
		con_add.setCountry("Canada");
		con_add.setPincode(56783);
		
		conor.setAddress(con_add);
		con_add.setEmployee(conor);
		
		Employee shiraz = new Employee();
		shiraz.setEname("Shriaz");
		shiraz.setEmail("shiraz@synechron.us");
		Address shiraz_add = new Address();
		shiraz_add.setCity("Miami");
		shiraz_add.setState("Florida");
		shiraz_add.setCountry("USA");
		shiraz_add.setPincode(56733);
		
		shiraz.setAddress(shiraz_add);
		shiraz_add.setEmployee(shiraz);
		
		Employee mehrsa = new Employee();
		mehrsa.setEname("Mehrsa");
		mehrsa.setEmail("mehrsa@synechron.eg");
		Address mehrsa_add = new Address();
		mehrsa_add.setCity("Cairo");
		mehrsa_add.setState("Cairo");
		mehrsa_add.setCountry("Egypt");
		mehrsa_add.setPincode(56356);
		
		mehrsa.setAddress(mehrsa_add);
		mehrsa_add.setEmployee(mehrsa);

		session.persist(conor);
		session.persist(shiraz);
		session.persist(mehrsa);
		transact.commit();
		session.close();
		System.out.println("Done Done Done");
	}
}
